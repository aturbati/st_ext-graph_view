package it.uniroma2.art.services;



import java.util.ArrayList;

import it.uniroma2.art.semanticturkey.exceptions.HTTPParameterUnspecifiedException;
import it.uniroma2.art.semanticturkey.exceptions.NonExistingRDFResourceException;
import it.uniroma2.art.semanticturkey.plugin.extpts.ServiceAdapter;
import it.uniroma2.art.semanticturkey.servlet.Response;
import it.uniroma2.art.semanticturkey.servlet.ServiceVocabulary.RepliesStatus;
import it.uniroma2.art.semanticturkey.servlet.XMLResponseREPLY;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.query.GraphQuery;
import it.uniroma2.art.owlart.query.MalformedQueryException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;
@Component
public class Graph extends ServiceAdapter {

	// PARS
	
	static final public String def = "init";
	public final static String jsonPar = "json";
	public final static String inferPar = "infer";
	public final static String resolveQueryRequest = "resolveQuery";
	public final static String resultTypeAttr = "resulttype";
	public JSONArray idArray=new JSONArray();

	
	boolean showMore = false;
	
	protected static Logger logger = LoggerFactory.getLogger(Graph.class);
	Integer moreCounter = 0;
	// Requests definition
	public static final String graphRequest = "graph";

	// Parameters of the requests.
	static final public String valueField = "value";

	String rx="";
	@Autowired
	public Graph(@Value("graphService") String id) {
		super(id);
	}
	@Override
	protected Response getPreCheckedResponse(String request) throws HTTPParameterUnspecifiedException  {
		
		String jsonString = setHttpPar(jsonPar);
		showMore = false;
		logger.debug("request to Graph Service");
		Response response = null;
		if (request == null)
		{
			return servletUtilities.createNoSuchHandlerExceptionResponse(request);
		}
		// Statistics
		if (request.equals(graphRequest)) {	
			try {
				response = graphMethod(jsonString);
			} catch (QueryEvaluationException e) {
				e.printStackTrace();
			} catch (ModelAccessException e) {
				e.printStackTrace();
			} catch (UnsupportedQueryLanguageException e) {
				e.printStackTrace();
			} catch (NonExistingRDFResourceException e) {
				e.printStackTrace();
			} catch (MalformedQueryException e) {
				e.printStackTrace();
			}
		}
		

		else
			return servletUtilities.createNoSuchHandlerExceptionResponse(request);
		
		return response;
	}
	

	/**
	 * Graph method
	 * @param showSubClassess 
	 * @param showIstances 
	 * @param showDataTypeProperties 
	 * @param showObjProperties 
	 * @param more 
	 * 
	 * @param value
	 * @return
	 * @throws JSONException 
	 * @throws UnloadableModelConfigurationException 
	 * @throws UnsupportedModelConfigurationException 
	 * @throws ModelCreationException 
	 * @throws MalformedQueryException 
	 * @throws ModelAccessException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 * @throws NonExistingRDFResourceException 
	 */

	
	public Response graphMethod(String jsonString ) 
			throws QueryEvaluationException, ModelAccessException, 
			UnsupportedQueryLanguageException, NonExistingRDFResourceException, MalformedQueryException 
	{

		// Creation of the reply.
		JSONObject json=null;
		try {
			json = new JSONObject(jsonString);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		String uri;
		try {
			uri = json.getString("input");
		} catch (JSONException e1) {
			uri = "";
		}
		boolean more;
		try {
			more = json.getBoolean("more");
		} catch (JSONException e1) {
			more = false;
		}
		String languageType;
		try {
			languageType = json.getString("lang");
		} catch (JSONException e1) {
			languageType = "";
		}
		boolean showIstances;
		try {
			showIstances = json.getBoolean("istances");
		} catch (JSONException e1) {
			showIstances = false;
		}
		boolean showObjProperties;
		try {
			showObjProperties = json.getBoolean("objData");
		} catch (JSONException e2) {
			showObjProperties = false;
		}
		boolean showDataTypeProperties;
		try {
			showDataTypeProperties = json.getBoolean("dataType");
		} catch (JSONException e1) {
			showDataTypeProperties = false;
		}
		boolean showSubClasses;
		try {
			showSubClasses = json.getBoolean("subclasses");
		} catch (JSONException e1) {
			showSubClasses=false;
		}
		boolean showDomainsAndRanges;
		try {
			showDomainsAndRanges = json.getBoolean("domainsAndRanges");
		} catch (JSONException e1) {
			showDomainsAndRanges=false;
		}
		boolean isClass;
		try {
			isClass = json.getBoolean("isClass");
		} catch (JSONException e1) {
			isClass=false;
		}
		boolean isScheme;
		try {
			isScheme = json.getBoolean("isScheme");
		} catch (JSONException e1) {
			isScheme=false;
		}
		boolean prefLabels;
		try {
			prefLabels = json.getBoolean("prefLabels");
		} catch (JSONException e1) {
			prefLabels=false;
		}
		boolean altLabels;
		try {
			altLabels = json.getBoolean("altLabels");
		} catch (JSONException e1) {
			altLabels=false;
		}
		boolean hiddenLabels;
		try {
			hiddenLabels = json.getBoolean("hiddenLabels");
		} catch (JSONException e1) {
			hiddenLabels=false;
		}
		boolean labelsAsNodes;
		try {
			labelsAsNodes = json.getBoolean("labelsAsNodes");
		} catch (JSONException e1) {
			labelsAsNodes=false;
		}
		boolean labelRelations;
		try  {
			 labelRelations = json.getBoolean("labelRelations");
		} catch (JSONException e1) {
			labelRelations = false;
		}

		String languageTag;
		try {
			languageTag = json.getString("langTag");
		} catch (JSONException e1) {
			languageTag="";
		}
		
		
		XMLResponseREPLY rx=null; 
		String q="";
		rx = servletUtilities.createReplyResponse(resolveQueryRequest, RepliesStatus.ok);
		//RDFSModel ontModel = (RDFSModel) ProjectManager.getCurrentProject().getOntModel();
		RDFModel ontModel = getProject().getOntModel();
		Element dataElement = ((XMLResponseREPLY) rx).getDataElement();
		dataElement.setAttribute(resultTypeAttr, "graph");
		if(languageType.equals("OWL"))
			/***************
			 * START OWL
			 * QUERY CREATION
			 * AND RESPONSE
			 * MANAGEMENT
			 * *************
			 */
		{
			if(isClass)//I need a query to get the subclasses
			{
				q=buildQueryAsObject(uri, more, showIstances, showSubClasses, showDataTypeProperties, 
						showObjProperties, showDomainsAndRanges);
				
				JSONArray arr= new JSONArray();
				GraphQuery query;
				try {
					query = ontModel.createGraphQuery(q);
				} catch (MalformedQueryException e) {
					query = null;
				}
				try {
					arr = buildJsonAsObject(query);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
				dataElement.setAttribute("result", arr.toString());
				JSONArray queryResults = new JSONArray();
	
				if(query!=null)
				{
					ARTStatementIterator statIt = (query).evaluate(false);
					int i=0;
					ARTStatement stat;
					while(statIt.hasNext())
					{
						JSONArray innerResults = new JSONArray();
		
					    stat = statIt.getNext();
						String pred = stat.getPredicate().toString();
						String obj = stat.getObject().toString();
						String subj = stat.getSubject().toString();
						int lastPred=pred.lastIndexOf('#');
						int lastSub= subj.lastIndexOf('#');
						int lastObj= obj.lastIndexOf('#');
						try {
							innerResults.put(0, subj.substring(lastSub+1)+ " "+ pred.substring(lastPred+1) + " "+ obj.substring(lastObj+1));
						} catch (JSONException e) {
							e.printStackTrace();
						}	
		
						try {
							queryResults.put(i, innerResults);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						i++;
					}
				}
				dataElement.setAttribute("info", queryResults.toString());
				dataElement.setAttribute("idList", idArray.toString());

			}
	
			else
			{
				q=buildQueryAsSubject(uri, more, showIstances, showSubClasses, showDataTypeProperties,
						showObjProperties, showDomainsAndRanges);
				if(!q.equals(""))
				{
					JSONArray arr= new JSONArray();
					GraphQuery query;
					try {
						query = ontModel.createGraphQuery(q);
					} catch (MalformedQueryException e) {
							query=null;
					}
					try {
						arr = buildJsonAsSubject(query);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					dataElement.setAttribute("result", arr.toString());
					ARTStatementIterator statIt = (query).evaluate(false);
					JSONArray queryResults = new JSONArray();
					int i=0;
					ARTStatement stat;
					while(statIt.hasNext())
					{
						JSONArray innerResults = new JSONArray();
	
					    stat = statIt.getNext();
						String pred = stat.getPredicate().asURIResource().getLocalName();
						String obj ="";

						if(stat.getObject().isURIResource())
						{
							obj = stat.getObject().asURIResource().getLocalName();
						}
						else
						{
							obj = stat.getObject().asLiteral().getLabel();
						}
						String subj ="";

						if(stat.getSubject().isURIResource())
						{
							subj = stat.getSubject().asURIResource().getLocalName();
						}
						else
						{
							subj = stat.getSubject().asLiteral().getLabel();
						}
						try {
							innerResults.put(0, subj+ " "+ pred + " "+ obj);
						} catch (JSONException e) {
							e.printStackTrace();
						}
	
						try {
							queryResults.put(i, innerResults);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						i++;
					}
					dataElement.setAttribute("info", queryResults.toString());
					dataElement.setAttribute("idList", idArray.toString());

	
				}
			}
		}
		else if(languageType.equals("SKOS"))
		/******************
		 * SKOS
		 * QUERY MANAGEMENT
		 *****************/
		{
			q=buildSKOSQuery(uri, more, isScheme, showObjProperties, showDataTypeProperties, prefLabels, altLabels, hiddenLabels);
			JSONArray arr= new JSONArray();
			GraphQuery query;
			try {
				query = ontModel.createGraphQuery(q);
			} catch (MalformedQueryException e) {
				query = null;
			}
			
			try {
				arr = buildSKOSJson(query, uri, languageTag, more, labelsAsNodes, labelRelations, false);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			dataElement.setAttribute("result", arr.toString());
			ARTStatementIterator statIt = (query).evaluate(false);
			JSONArray queryResults = new JSONArray();
			int i=0;
			ARTStatement stat;
			while(statIt.hasNext())
			{
				JSONArray innerResults = new JSONArray();

			    stat = statIt.getNext();
				String pred = stat.getPredicate().asURIResource().getLocalName();
				String obj ="";

				if(stat.getObject().isURIResource())
				{
					obj = getPrefLabel(stat.getObject().asURIResource().getURI(),languageTag, false)[0];
					if(obj=="")
						obj=stat.getObject().asURIResource().getLocalName();

				}
				else
				{
					obj = stat.getObject().asLiteral().getLabel();
				}
				String subj ="";

				if(stat.getSubject().isURIResource())
				{
					subj = getPrefLabel(stat.getSubject().asURIResource().getURI(),languageTag, false)[0];
					if(subj=="")
						subj=stat.getSubject().asURIResource().getLocalName();
				}
				else
				{
					subj = stat.getSubject().asLiteral().getLabel();
				}
				try {
					innerResults.put(0, subj+ " "+ pred + " "+ obj);
				} catch (JSONException e) {
					e.printStackTrace();
				}

				try {
					queryResults.put(i, innerResults);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				i++;
			}
			dataElement.setAttribute("info", queryResults.toString());
			dataElement.setAttribute("idList", idArray.toString());


		}
		else if(languageType.equals("SKOSXL"))
		{
		/******************
		 * SKOSXL
		 * QUERY MANAGEMENT
		 *****************/
		q=buildSKOSXLQuery(uri, more, isScheme, showObjProperties, showDataTypeProperties, prefLabels, altLabels, hiddenLabels);
		JSONArray arr= new JSONArray();
		GraphQuery query;
		try {
			query = ontModel.createGraphQuery(q);
		} catch (MalformedQueryException e) {
			query = null;
		}
		
		try {
			arr = buildSKOSJson(query, uri, languageTag, more, labelsAsNodes, labelRelations, true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		dataElement.setAttribute("result", arr.toString());
		ARTStatementIterator statIt = (query).evaluate(false);
		JSONArray queryResults = new JSONArray();
		int i=0;
		ARTStatement stat;
		while(statIt.hasNext())
		{
			JSONArray innerResults = new JSONArray();

		    stat = statIt.getNext();
			String pred = stat.getPredicate().asURIResource().getLocalName();
			String obj ="";

			if(stat.getObject().isURIResource())
			{
				obj = getPrefLabel(stat.getObject().asURIResource().getURI(),languageTag, true)[0];
				if(obj=="")
					obj=stat.getObject().asURIResource().getLocalName();

			}
			else
			{
				obj = stat.getObject().asLiteral().getLabel();
			}
			String subj ="";

			if(stat.getSubject().isURIResource())
			{
				subj = getPrefLabel(stat.getSubject().asURIResource().getURI(),languageTag, true)[0];
				if(subj=="")
					subj=stat.getSubject().asURIResource().getLocalName();
			}
			else
			{
				subj = stat.getSubject().asLiteral().getLabel();
			}
			try {
				innerResults.put(0, subj+ " "+ pred + " "+ obj);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			try {
				queryResults.put(i, innerResults);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			i++;
		}
		dataElement.setAttribute("info", queryResults.toString());
		dataElement.setAttribute("idList", idArray.toString());


		
		}

		return rx;	
	}
	

	private String buildSKOSQuery(String uri, boolean more, boolean isScheme, boolean showObjProperties, boolean showDataTypeProperties, boolean prefLabels, boolean altLabels, boolean hiddenLabels)
	{
	
		String q="";
		{		
			
				if(uri.equals("start"))//get schemes
				{
			
					q =		"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n"+
							"CONSTRUCT{?x ?y skos:ConceptScheme}\n"+
							"WHERE {?x ?y skos:ConceptScheme\n"+
							"FILTER (?x!= skos:Collection)\n"+
							"FILTER (?x!= skos:inScheme)\n"+
							"FILTER (?x!= skos:hasTopConcept)\n"+
							"FILTER (?x!= skos:topConceptOf)\n"+
							"}";
				}
				else if(isScheme)
				{
					//get top concepts of the selected scheme
				    q =		"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n"+
				    		"CONSTRUCT{?x skos:topConceptOf <"+uri+">}\n"+
				    		"WHERE {?x skos:topConceptOf <"+uri+">}";
				}
				else
				{
					q =		"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n"+
							"PREFIX owl:<http://www.w3.org/2002/07/owl#>"+
				    		"CONSTRUCT{?x ?y ?z} \n"+
				    		"WHERE{{?x ?y ?z. FILTER(?x = <"+uri+">) FILTER (?z != skos:Concept) FILTER(?y != skos:inScheme) FILTER(?y != skos:broader) FILTER(?y != skos:topConceptOf)";
				    		if(!showObjProperties)
				    		{ 
				    			q+="FILTER NOT EXISTS{?y a owl:ObjectProperty}.\n";
				    		}
				    		if(!showDataTypeProperties)
				    		{
				    			q+="FILTER NOT EXISTS{?y a owl:DatatypeProperty}.\n";
				    		}
				    		if(!prefLabels)
				    		{
				    			q+="FILTER(?y != skos:prefLabel).\n";
				    		}
				    		if(!altLabels)
				    		{
				    			q+="FILTER(?y != skos:altLabel).\n";
				    		}
				    		if(!hiddenLabels)
				    		{
				    			q+="FILTER(?y != skos:hiddenLabel).\n";
				    		}

				    	      q+="}\nUNION {?x ?y ?z. FILTER(?y = skos:broader) FILTER(?z = <"+uri+">) FILTER (?x != skos:Concept)}";
					    	  q+="}";				
					  
				}
				
			    
				
		}
		
		return q;
	}
	private String buildSKOSXLQuery(String uri, boolean more, boolean isScheme, boolean showObjProperties, boolean showDataTypeProperties, boolean prefLabels, boolean altLabels, boolean hiddenLabels)
	{
	
		String q="";		
			
			if(uri.equals("start"))//get schemes
			{
		
				q =		"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n"+
						"PREFIX skosxl:<http://www.w3.org/2008/05/skos-xl#>\n"+
						"CONSTRUCT{?x ?y skos:ConceptScheme}\n"+
						"WHERE {?x ?y skos:ConceptScheme\n"+
						"FILTER (?x!= skos:Collection)\n"+
						"FILTER (?x!= skos:inScheme)\n"+
						"FILTER (?x!= skos:hasTopConcept)\n"+
						"FILTER (?x!= skos:topConceptOf)\n"+
						"FILTER (?x!= skosxl:Label)\n"+
						"}";
			}
			else if(isScheme)
			{
				//get top concepts of the selected scheme
			    q =		"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n"+
			    		"CONSTRUCT{?x skos:topConceptOf <"+uri+">}\n"+
			    		"WHERE {?x skos:topConceptOf <"+uri+">}";
			}
			else
			{
				q =		"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n"+
						"PREFIX owl:<http://www.w3.org/2002/07/owl#>"+
			    		"CONSTRUCT{?x ?y ?z} \n"+
			    		"WHERE{{?x ?y ?z. FILTER(?x = <"+uri+">) FILTER (?z != skos:Concept) FILTER(?y != skos:inScheme) FILTER(?y != skos:broader) FILTER(?y != skos:topConceptOf)";
			    		if(!showObjProperties)
			    		{ 
			    			q+="FILTER NOT EXISTS{?y a owl:ObjectProperty}.\n";
			    		}
			    		if(!showDataTypeProperties)
			    		{
			    			q+="FILTER NOT EXISTS{?y a owl:DatatypeProperty}.\n";
			    		}
			    		if(!prefLabels)
			    		{
			    			q+="FILTER(?y != skos:prefLabel).\n";
			    		}
			    		if(!altLabels)
			    		{
			    			q+="FILTER(?y != skos:altLabel).\n";
			    		}
			    		if(!hiddenLabels)
			    		{
			    			q+="FILTER(?y != skos:hiddenLabel).\n";
			    		}
	
			            q+="}\nUNION {?x ?y ?z. FILTER(?y = skos:broader) FILTER(?z = <"+uri+">) FILTER (?x != skos:Concept)}";

			    		q+="}";

			}
			return q;
				
	}

	private String buildQueryAsSubject(String uri, boolean more, boolean showIstances, 
			boolean showSubClasses, boolean showDataTypeProperties, boolean showObjProperties, boolean showDomainsAndRanges) {
		String q="";
		if(uri.equals(def) || uri.equals("http://www.w3.org/2002/07/owl#Thing"))
			return "";
		if(more)
			showMore = true;
		
		/**********
		 *asSubject*
		 **********/
	
		q=		"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
				"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
				"PREFIX owl:<http://www.w3.org/2002/07/owl#>"+
				"CONSTRUCT {<"+uri+"> ?y ?z}\n"+
				"WHERE {<"+uri+"> ?y ?z.\n"+
				"FILTER(?z != owl:Class)";
				//if I don't want to..
				if(!showIstances)
				{
					q+=	"FILTER (?y != rdf:type)\n";
				}
				if(!showSubClasses)
				{
					q+= "FILTER (?y != rdfs:subClassOf)";
				}
				if(!showDataTypeProperties)
				{
					q+= "FILTER NOT EXISTS{?y a owl:DatatypeProperty}.\n";
				}
				if(!showObjProperties) 
				{
					q+="FILTER NOT EXISTS{?y a owl:ObjectProperty}.";
				}
				if(!showDomainsAndRanges)
				{
					 q+="FILTER (?y !=rdfs:domain)."+
					    "FILTER (?y !=rdfs:range).";
				}

				q+="}";	
		

		return q;
	}
	
	private boolean checkIfIstance(String subj)
	{
		String result =buildQueryAsSubject(subj, true, true, true, true,
				true, true);//i don't care about visualization options since I need only to check if there's a type relation to the subject
		if(result!="")
		{
			GraphQuery query;
			RDFModel ontModel = getProject().getOntModel();

			try
			{
				query = ontModel.createGraphQuery(result);
				ARTStatementIterator statIt = (query).evaluate(false);
				ARTStatement stat;
				while(statIt.hasNext())
				{
					
					stat=statIt.getNext();
					String pred = stat.getPredicate().asURIResource().getURI();
					if(pred.equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"))
						return true;
				}
			} catch (UnsupportedQueryLanguageException e) 
			{
				return false;
			} catch (ModelAccessException e)
			{
				return false;
			} catch (MalformedQueryException e) 
			{
				return false;
			} catch (QueryEvaluationException e) 
			{
				return false;
			}

		}
		return false;
	}
	private String buildQueryAsObject(String uri, boolean more, boolean showIstances, boolean showSubClasses, 
			boolean showDataTypeProperties, boolean showObjProperties, boolean showDomainsAndRanges) {
		String q="";
		 
			if(more)
				showMore = true;
			
			/**********
			 *asObject*
			 **********/
			if(uri.equals(def) || uri.equals("http://www.w3.org/2002/07/owl#Thing"))
			{
				q=	  "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
					  "PREFIX owl:<http://www.w3.org/2002/07/owl#>"+
					  "CONSTRUCT {?x rdfs:subClassOf owl:Thing}"+
					  "WHERE {?x rdfs:subClassOf owl:Thing.}";
			}
			else
			{				
				q=		"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
						"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
						"PREFIX owl:<http://www.w3.org/2002/07/owl#>"+
						"CONSTRUCT {?x ?y <"+uri+">}\n"+
						"WHERE {?x ?y <"+uri+">.\n";

						if(!showIstances)
						{
							q+=	"FILTER (?y != rdf:type)\n";
						}
						if(!showSubClasses)
						{
							q+= "FILTER (?y != rdfs:subClassOf)";
						}
						if(!showDataTypeProperties)
						{
							q+= "FILTER NOT EXISTS{?y a owl:DatatypeProperty}.\n";
						}
						if(!showObjProperties) 
						{
							q+="FILTER NOT EXISTS{?y a owl:ObjectProperty}.";
						}
						if(!showDomainsAndRanges)
						{
							 q+="FILTER (?y !=rdfs:domain)."+
							    "FILTER (?y !=rdfs:range).";

						}
						q+="}";
			}
		return q;
	}
	
	private JSONArray buildSKOSJson(GraphQuery query, String uri, String languageTag, boolean more,
			boolean showLabelsAsNodes, boolean labelRelations, boolean skosxl) throws JSONException, QueryEvaluationException, ModelAccessException, NonExistingRDFResourceException, UnsupportedQueryLanguageException, MalformedQueryException 
	{
		idArray = null;
		idArray= new JSONArray();
		JSONArray arr = new JSONArray();
	
		if(query==null)
		{
			JSONObject noResults = new JSONObject();
			noResults.put("$noResults", "true");
			arr.put(0, noResults);
			return arr;
		}
		int i = 0;

		ARTStatementIterator statIt = (query).evaluate(false);
		if(!statIt.hasNext())
		{
			JSONObject noResults = new JSONObject();
			noResults.put("$noResults", "true");
			arr.put(0, noResults);
			return arr;
		}

		else if(uri.equals("start"))//build json to show all the avaiable schemes
		{
			//i need a fake node to link schemes
			JSONObject fake = new JSONObject();
			JSONObject opt = new JSONObject();
			JSONArray adj = new JSONArray();
			JSONObject edgeOpt = new JSONObject();
			edgeOpt.put("$color", "#FFFFFF");
			fake.put("id", "fake");
			fake.put("name", "");
			opt.put("$color", "#FFFFFF");
			opt.put("$dim", "8");
			fake.put("data", opt);
			fake.put("adjacencies",adj);
			arr.put(i, fake);
			i++;
			while(statIt.hasNext())//linking all the schemes to fake in this while
			{				
		
				ARTStatement stat;
				JSONObject subNode = new JSONObject();
				JSONObject dataSub = new JSONObject();
				JSONArray adjSub = new JSONArray();
				JSONObject optSub = new JSONObject();
				optSub.put("$scheme", "true");
				stat = statIt.getNext();
				//connect subjects (concept Schemes) to fake 
				String subj = stat.getSubject().asURIResource().getURI();
				String subjLocName = stat.getSubject().asURIResource().getLocalName();
				dataSub.put("nodeTo", "fake");
				dataSub.put("nodeFrom", subj);
				dataSub.put("data", edgeOpt);
				adjSub.put(0,dataSub);
				subNode.put("adjacencies",adjSub);
				subNode.put("data", optSub);
				subNode.put("id",subj);
				subNode.put("name", subjLocName);
				arr.put(i, subNode);
				idArray.put(subj);
		
				
				i++;

			}
		}
		
		else
		{
			
			ARTStatement stat = null;
			Integer counter =0;
						
			JSONArray adj = new JSONArray();
			JSONObject empty = new JSONObject();
			counter = 0;
			int innerCounter =0;
			ArrayList<ARTStatement> statements = new ArrayList<ARTStatement>();
			ArrayList<String> labelRelatedSubjects = new ArrayList<String>();
			while(statIt.hasNext()) 
			{
				statements.add(statIt.getNext());
			}
			while(innerCounter<statements.size() && (counter <10 || more))
			{
				counter++;
			    stat=statements.get(innerCounter);
				innerCounter++;
				JSONObject subNode = new JSONObject();
				JSONArray adjSub = new JSONArray();
				JSONObject optSub = new JSONObject();
				JSONObject dataSub = new JSONObject();
				JSONObject edgeOpt = new JSONObject(); 
				JSONArray direction = new JSONArray();
				String obj = "";
				String subj = "";
				String objLocName = "";
				String subjLocName = "";

				if(stat.getSubject().isURIResource())
				{
					subj = stat.getSubject().asURIResource().getURI();
					if(labelRelations && skosxl && !labelRelatedSubjects.contains(subj))//only in SKOSXL
					{
						labelRelatedSubjects.add(subj);
						ArrayList<ARTStatement> labelStat = getRelation(subj);
						statements.addAll(labelStat);

					}
					subjLocName = getPrefLabel(subj, languageTag, skosxl)[0];
					if(subjLocName=="")
						subjLocName = stat.getSubject().asURIResource().getLocalName();
				

					if(stat.getObject().isURIResource())
					{
						obj = stat.getObject().asURIResource().getURI();
						objLocName = getPrefLabel(obj, languageTag, skosxl)[0];
						if(objLocName=="")
							objLocName = stat.getObject().asURIResource().getLocalName();
						
					}
					else
					{
						obj = stat.getObject().asLiteral().getLabel();
						objLocName = getPrefLabel(obj, languageTag, skosxl)[0];
						if(objLocName=="")
							objLocName = stat.getObject().asLiteral().getNominalValue();
					}

					
				}
				else
				{
					subj = stat.getSubject().asLiteral().getLabel();
					subjLocName = stat.getSubject().asLiteral().getNominalValue();
				}

				String predLocName = stat.getPredicate().getLocalName();
				if(!showLabelsAsNodes && (predLocName.equals("prefLabel")
						|| predLocName.equals("altLabel")
						|| predLocName.equals("hiddenLabel")))
				{
					counter--;
				}
				else
				{
					JSONArray predicateArray = new JSONArray();
					predicateArray.put(0, subjLocName);
					predicateArray.put(1, predLocName);
					predicateArray.put(2, objLocName);
					optSub.put("$predicate"+counter.toString(),predicateArray);
	
					if(predLocName.equals("broader") || predLocName.equals("narrower") || predLocName.equals("topConceptOf"))
					{
						optSub.put("$overridable", "true");
						optSub.put("$type", "circle");
						optSub.put("$dim", "9");
						optSub.put("$color", "#5eaba6");
						edgeOpt.put("$color", "#003366");
					}
					else if(predLocName.equals("prefLabel"))
					{
						optSub.put("$overridable", "true");
						optSub.put("$type", "circle");
						optSub.put("$dim", "9");
						optSub.put("$color", "#3333CC");
						edgeOpt.put("$color", "#6699FF");
	
					}
					else if(predLocName.equals("altLabel"))
					{
						optSub.put("$overridable", "true");
						optSub.put("$type", "square");
						optSub.put("$dim", "9");
						optSub.put("$color", "#663300");	
						edgeOpt.put("$color", "#7A5229");
	
					}
					else if(predLocName.equals("hiddenLabel"))
					{
						optSub.put("$overridable", "true");
						optSub.put("$type", "square");
						optSub.put("$dim", "9");
						optSub.put("$color", "#003300");	
						edgeOpt.put("$color", "#666633");
	
					}
					else if(predLocName.equals("labelRelation"))
					{
						optSub.put("$overridable", "true");
						optSub.put("$type", "square");
						optSub.put("$dim", "9");
						optSub.put("$color", "#00d9e5");	
						edgeOpt.put("$color", "#006B00");
						
					}
					else
					{
						optSub.put("$overridable", "true");
						optSub.put("$type", "square");
						optSub.put("$dim", "9");
						optSub.put("$color", "#6600CC");	
						edgeOpt.put("$color", "#8A5CE6");
					}
					optSub.put("$parent", uri);
					if(uri.equals(subj))
					{
						dataSub.put("nodeTo", subj);
						dataSub.put("nodeFrom", obj);
					}
					else
					{
						dataSub.put("nodeTo", obj);
						dataSub.put("nodeFrom", subj);
					}
	
	
					direction.put(0, subj);
					direction.put(1, obj);
					edgeOpt.put("$direction", direction);
	
					dataSub.put("data", edgeOpt);
					adjSub.put(0,dataSub);
					adj.put(counter-1,dataSub);
	
					subNode.put("adjacencies",adjSub);
					subNode.put("data",optSub);
					if(uri.equals(subj))
					{
						subNode.put("id",obj);
						subNode.put("name",objLocName);
						idArray.put(obj);
					}
					else
					{
						subNode.put("id",subj);
						subNode.put("name",subjLocName);
						idArray.put(subj);
					}
					
					arr.put(i, subNode);
					
					i++;
					if(counter>=10 && !more)
					{			
						JSONObject showMoreNode = new JSONObject();
						JSONArray adjMore = new JSONArray();
						JSONObject optMore = new JSONObject();
						JSONObject dataMore = new JSONObject();
						optMore.put("$dim", "9");
						optMore.put("$type", "square");
						showMoreNode.put("id", "More"+moreCounter.toString());
						showMoreNode.put("name", "More");
						
						if(uri.equals(subj))
						{
							optMore.put("$parent", stat.getSubject().asURIResource().getURI());
							dataMore.put("nodeTo", stat.getSubject().asURIResource().getURI());
						}
						else
						{
							optMore.put("$parent", stat.getObject().asURIResource().getURI());
							dataMore.put("nodeTo", stat.getObject().asURIResource().getURI());

						}
						dataMore.put("nodeFrom", "More"+moreCounter.toString());
						dataMore.put("data", empty);

						adjMore.put(0, dataMore);
						showMoreNode.put("adjacencies", adjMore);
						showMoreNode.put("data", optMore);
						arr.put(i, showMoreNode);
						moreCounter++;
						i++;
						
					}

				}
			}

			
		}
		return arr;
	}
	
	private ArrayList<ARTStatement> getRelation(String id) throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException, QueryEvaluationException 
	{
		String q = "PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n"+
				 "PREFIX skosxl:<http://www.w3.org/2008/05/skos-xl#>\n"+
                 "CONSTRUCT {<"+id+"> skosxl:labelRelation ?w}\n"+
				 "WHERE{\n"+
				 "<"+id+"> skosxl:prefLabel ?a.\n"+
			     "?a skosxl:labelRelation ?b.\n"+
				 "?w skosxl:prefLabel ?b}";
		RDFModel ontModel = getProject().getOntModel();
		GraphQuery query = ontModel.createGraphQuery(q);
		ARTStatementIterator statIt = (query).evaluate(false);
		ARTStatement tempStat;
		ArrayList<ARTResource> subjArray = new ArrayList<ARTResource>(); 
		ArrayList<ARTURIResource> predArray = new ArrayList<ARTURIResource>(); 
		ArrayList<ARTNode> objArray = new ArrayList<ARTNode>(); 

		while(statIt.hasNext())
		{
			tempStat = statIt.getNext();
			subjArray.add(tempStat.getSubject());
			predArray.add(tempStat.getPredicate());
			objArray.add(tempStat.getObject());
		}
		
		ArrayList<ARTStatement> relationStat = new ArrayList<ARTStatement>();
		int i=0;
		while(i<subjArray.size())
		{
			tempStat = ontModel.createStatement(subjArray.get(i), predArray.get(i), objArray.get(i));
			relationStat.add(tempStat);
			i++;
		}
		return relationStat;
	}
	private String[] getPrefLabel(String uri, String langTag, boolean skosxl) throws UnsupportedQueryLanguageException, ModelAccessException, QueryEvaluationException 
	{
		String q="";
		if(!skosxl)
		{
			q= "PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n"+
					  "CONSTRUCT {<"+uri+"> skos:prefLabel ?z}\n"+
					  "WHERE{<"+uri+"> skos:prefLabel ?z.\n"+
					  "FILTER(langMatches(lang(?z), \""+langTag+"\"))}";
		}
		else
		{
			q= "PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n"+
			   "PREFIX skosxl:<http://www.w3.org/2008/05/skos-xl#>\n"+
					  "CONSTRUCT {?y skosxl:prefLabel ?z}\n"+
					  "WHERE{<"+uri+"> skosxl:prefLabel ?y.\n"+
					  "?y skosxl:literalForm ?z."+
					  "FILTER(langMatches(lang(?z), \""+langTag+"\"))}";
		}
		RDFModel ontModel = getProject().getOntModel();
		GraphQuery query;
		try {
			query = ontModel.createGraphQuery(q);
		} catch (MalformedQueryException e) {
			query = null;
		}
		ARTStatementIterator statIt;
		if(query!=null)
			statIt = (query).evaluate(false);
		else
		{
			statIt = null;
		}
		String toReturn[]={"",""};
		ARTStatement stat;
		if(statIt!=null)
		{
			if(statIt.hasNext())
			{
				stat=statIt.getNext();
				toReturn[0] = stat.getObject().getNominalValue();
				if(skosxl)
				{
					toReturn[1]=stat.getSubject().asURIResource().getURI();
				}
			}
		}
		return toReturn;
	}
	
	private JSONArray buildJsonAsObject(GraphQuery query) throws JSONException, QueryEvaluationException, ModelAccessException {
		idArray=null;
		idArray = new JSONArray();
		JSONArray arr= new JSONArray();
		if(query==null)
		{
			JSONObject noResults = new JSONObject();
			noResults.put("$noResults", "true");
			arr.put(0, noResults);
			return arr;
		}
		
		ARTStatementIterator statIt = (query).evaluate(false);
		ARTStatementIterator subIt = (query).evaluate(false);
		if(!statIt.hasNext())
		{
			JSONObject noResults = new JSONObject();
			noResults.put("$noResults", "true");
			arr.put(0, noResults);
			return arr;
		}
		int i=0;
		ARTStatement stat = null;
		Integer counter =0;
	
		JSONObject json=new JSONObject();
		stat = statIt.getNext();

		/**********
		 * STAT: <soggetto> <predicato> <oggetto>
		 **********/
			 
		/********************
		 * NODO DI PARTENZA *
		 *******************/
		JSONArray adj = new JSONArray();
		JSONObject opt = new JSONObject();
		JSONObject empty = new JSONObject();
		String startObj = "";
		String startObjLocName = "";
		if(stat.getObject().isURIResource())
		{
			 startObj = stat.getObject().asURIResource().getURI();
			 startObjLocName = stat.getObject().asURIResource().getLocalName();
		}
		else
		{
			startObj = stat.getObject().asLiteral().getLabel();
			startObjLocName = stat.getObject().getNominalValue();
			
		}
		json.put("id",startObj); //id: URI del nodo
	    json.put("name",startObjLocName);//nome
	    idArray.put(startObj);
	    /*
	     * Dati per la visualizzazione grafica
	     */
		opt.put("$color", "#EBB056");
		opt.put("$dim", "11");
		opt.put("$parent", stat.getObject());
		json.put("data",opt);

		json.put("adjacencies",adj);
		arr.put(i,json);
		i++;

		ARTStatement appStat = stat;
		counter = 0;
		while(subIt.hasNext() && (counter <10 || showMore))
		{
			counter++;
			stat=subIt.getNext();
			JSONObject subNode = new JSONObject();
			JSONArray adjSub = new JSONArray();
			JSONObject optSub = new JSONObject();
			JSONObject dataSub = new JSONObject();
			JSONObject edgeOpt = new JSONObject(); 
			JSONArray direction = new JSONArray();
			String obj = "";
			String subj = "";
			String predLocName = "";
			String objLocName = "";
			String subjLocName = "";
			if(stat.getObject().isURIResource())
			{
				obj= stat.getObject().asURIResource().getURI();
				objLocName = stat.getObject().asURIResource().getLocalName();

			}
			else
			{
				obj= stat.getObject().asLiteral().getLabel();
				objLocName = stat.getObject().asLiteral().getNominalValue();

			}
			if(stat.getSubject().isURIResource())
			{
				subj = stat.getSubject().asURIResource().getURI();
				subjLocName = stat.getSubject().asURIResource().getLocalName();

			}
			else
			{
				subj = stat.getSubject().asLiteral().getLabel();
				subjLocName = stat.getSubject().asLiteral().getNominalValue();

			}
			
			predLocName = stat.getPredicate().getLocalName();			
			JSONArray predicateArray = new JSONArray();
			predicateArray.put(0, subjLocName);
			predicateArray.put(1, predLocName);
			predicateArray.put(2, objLocName);
			optSub.put("$predicate"+counter.toString(),predicateArray);

			if(predLocName.equals("subClassOf") )
			{
				optSub.put("$overridable", "true");
				optSub.put("$color", "#EBB056");
				optSub.put("$dim", "11");
				optSub.put("$type", "circle");
				edgeOpt.put("$color", "#545f8b");
			}
			else if(predLocName.equals("type") || checkIfIstance(subj) )
			{
				optSub.put("$overridable", "true");
				optSub.put("$color", "#83548B");
				optSub.put("$dim", "9");
				optSub.put("$type", "square");
				optSub.put("$istance", "true");
				edgeOpt.put("$color", "#1ec908");
			}

			else
			{
				optSub.put("$overridable", "true");
				optSub.put("$color", "#608b54");
				optSub.put("$type", "star");
				optSub.put("$dim", "9");
				edgeOpt.put("$color", "#f71b1b");
			}
			optSub.put("$parent", obj);
			dataSub.put("nodeTo", obj);
			dataSub.put("nodeFrom", subj);
			direction.put(0, subj);
			direction.put(1, obj);
			edgeOpt.put("$direction", direction);
			dataSub.put("data", edgeOpt);
			adjSub.put(0,dataSub);
			subNode.put("adjacencies",adjSub);
			subNode.put("data",optSub);
			subNode.put("id",subj);
			idArray.put(subj);
			

			subNode.put("name", subjLocName);
			
			arr.put(i, subNode);
			
			i++;
		}
		
		if(counter>=10 && !showMore)
		{			
			JSONObject showMoreNode = new JSONObject();
			JSONArray adjMore = new JSONArray();
			JSONObject optMore = new JSONObject();
			JSONObject dataMore = new JSONObject();
			optMore.put("$dim", "9");
			optMore.put("$type", "square");

			optMore.put("$parent", appStat.getObject());

			showMoreNode.put("id", "More"+moreCounter.toString());
			showMoreNode.put("name", "More");
			dataMore.put("nodeTo", appStat.getObject());
			dataMore.put("nodeFrom", "More"+moreCounter.toString());
			dataMore.put("data", empty);
			adjMore.put(0, dataMore);
			showMoreNode.put("adjacencies", adjMore);
			showMoreNode.put("data", optMore);
			arr.put(i, showMoreNode);
			moreCounter++;
			i++;
			
		}

		return arr;
	}
	private JSONArray buildJsonAsSubject(GraphQuery query) throws JSONException, QueryEvaluationException, ModelAccessException {
		idArray=null;
		idArray=new JSONArray();
		JSONArray arr= new JSONArray();
		if(query==null)
		{
			JSONObject noResults = new JSONObject();
			noResults.put("$noResults", "true");
			arr.put(0, noResults);
			return arr;

		}

		ARTStatementIterator statIt = (query).evaluate(false);
		ARTStatementIterator subIt = (query).evaluate(false);
		if(!statIt.hasNext())
		{
			JSONObject noResults = new JSONObject();
			noResults.put("$noResults", "true");
			arr.put(0, noResults);
			return arr;
		}
		int i=0;
		ARTStatement stat = null;
		Integer counter =0;
	
		JSONObject json=new JSONObject();
		stat = statIt.getNext();

		/**********
		 * STAT: <soggetto> <predicato> <oggetto>
		 **********/
			 
		/********************
		 * NODO DI PARTENZA *
		 *******************/
		JSONArray adj = new JSONArray();
		JSONObject opt = new JSONObject();
		JSONObject empty = new JSONObject();
		String startSubj = "";
		String startSubjLocName = "";
		if(stat.getSubject().isURIResource())
		{
			startSubj = stat.getSubject().asURIResource().getURI();
			startSubjLocName = stat.getSubject().asURIResource().getLocalName();
		}
		else
		{
			startSubj = stat.getSubject().asLiteral().getLabel();
			startSubjLocName = stat.getSubject().asLiteral().getNominalValue();
		}
		json.put("id",startSubj); //id: URI del nodo
	    json.put("name",startSubjLocName);//nome
	    idArray.put(startSubj);
	    /*
	     * Dati per la visualizzazione grafica
	     */
		opt.put("$color", "#EBB056");
		opt.put("$dim", "9");
		opt.put("$parent", stat.getObject().asURIResource().getURI());
		json.put("data",opt);

		arr.put(i,json);
		i++;

		ARTStatement appStat = stat;
		counter = 0;
		while(subIt.hasNext() && (counter <10 || showMore))
		{
			counter++;
			stat=subIt.getNext();
			JSONObject subNode = new JSONObject();
			JSONArray adjSub = new JSONArray();
			JSONObject optSub = new JSONObject();
			JSONObject dataSub = new JSONObject();
			JSONObject edgeOpt = new JSONObject(); 
			JSONArray direction = new JSONArray();
			String obj = "";
			String subj = "";
			String predLocName = "";
			String objLocName = "";
			String subjLocName = "";
			if(stat.getObject().isURIResource())
			{
				obj= stat.getObject().asURIResource().getURI();
				objLocName = stat.getObject().asURIResource().getLocalName();

			}
			else
			{
				obj= stat.getObject().asLiteral().getLabel();
				objLocName = stat.getObject().asLiteral().getNominalValue();

			}
			if(stat.getSubject().isURIResource())
			{
				subj = stat.getSubject().asURIResource().getURI();
				subjLocName = stat.getSubject().asURIResource().getLocalName();

			}
			else
			{
				subj = stat.getSubject().asLiteral().getLabel();
				subjLocName = stat.getSubject().asLiteral().getNominalValue();

			}
			
			predLocName = stat.getPredicate().getLocalName();			
			JSONArray predicateArray = new JSONArray();
			predicateArray.put(0, subjLocName);
			predicateArray.put(1, predLocName);
			predicateArray.put(2, objLocName);
			optSub.put("$predicate"+counter.toString(),predicateArray);

			if(predLocName.equals("type") )
			{
				optSub.put("$overridable", "true");
				optSub.put("$color", "#EBB056");
				optSub.put("$dim", "9");
				optSub.put("$type", "circle");
				edgeOpt.put("$color", "#1ec908");


			}
			else if(checkIfIstance(obj))
			{

				optSub.put("$overridable", "true");
				optSub.put("$color", "#83548B");
				optSub.put("$dim", "9");
				optSub.put("$type", "square");
				optSub.put("$istance", "true");
				edgeOpt.put("$color", "#e87a12");

			}
			else
			{
				optSub.put("$overridable", "true");
				optSub.put("$color", "#608b54");
				optSub.put("$type", "star");
				optSub.put("$dim", "9");
				edgeOpt.put("$color", "#f71b1b");

			}
			optSub.put("$parent", obj);
			dataSub.put("nodeTo", obj);
			dataSub.put("nodeFrom", subj);
			direction.put(0, subj);
			direction.put(1, obj);
			edgeOpt.put("$direction", direction);

			dataSub.put("data", edgeOpt);
			adjSub.put(0,dataSub);
			adj.put(counter-1,dataSub);

			subNode.put("adjacencies",adjSub);
			subNode.put("data",optSub);
			subNode.put("id",obj);
			idArray.put(obj);
			subNode.put("name", objLocName);
			
			arr.put(i, subNode);
			
			i++;
		}
		
		if(counter>=10 && !showMore)
		{			
			JSONObject showMoreNode = new JSONObject();
			JSONArray adjMore = new JSONArray();
			JSONObject optMore = new JSONObject();
			JSONObject dataMore = new JSONObject();
			optMore.put("$dim", "9");
			optMore.put("$type", "square");

			optMore.put("$parent", appStat.getObject());

			showMoreNode.put("id", "More"+moreCounter.toString());
			showMoreNode.put("name", "More");
			dataMore.put("nodeTo", appStat.getObject());
			dataMore.put("nodeFrom", "More"+moreCounter.toString());
			dataMore.put("data", empty);
			adjMore.put(0, dataMore);
			showMoreNode.put("adjacencies", adjMore);
			showMoreNode.put("data", optMore);
			arr.put(i, showMoreNode);
			moreCounter++;
			i++;
			
		}
		json.put("adjacencies",adj);


		return arr;
	}


	@Override
	protected Logger getLogger() {
		return null;
	}
}
		
			
	
/*
 *	FOR OWL 
 *	if I click on anything except an Istance:
 *
 * 	- send the result of the query having the URI of the class as object in the asObject field.
 * 
 *  if I click on an Istance
 *  
 *  - send the result of the query having the URI of the resource as subject in the asSubject field
 * 
 * */
