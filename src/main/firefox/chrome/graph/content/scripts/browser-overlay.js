/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * 
 * The Original Code is SemanticTurkey.
 * 
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 * 
 * SemanticTurkey was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART) Current
 * information about SemanticTurkey can be obtained at
 * http://semanticturkey.uniroma2.it
 * 
 */

if (typeof art_semanticturkey == 'undefined') 
	var art_semanticturkey = {};

Components.utils.import("resource://stmodules/Logger.jsm", art_semanticturkey);
Components.utils.import("resource://stmodules/stEvtMgr.jsm", art_semanticturkey);

//Function associated with graphButton
var type;
art_semanticturkey.graphButtonManager = function() {
	art_semanticturkey.Logger.debug("art_semanticturkey.evtMgr"+art_semanticturkey.evtMgr);
	art_semanticturkey.Logger.debug("art_semanticturkey.evtMgr.registerForEvent"+art_semanticturkey.evtMgr.registerForEvent);
	if(type=="OWL")
	{	
		art_semanticturkey.openUrl("chrome://graph/content/showGraph.html");	
	}
	else if(type=="SKOS")
	{
		art_semanticturkey.openUrl("chrome://graph/content/showGraphSKOS.html");	
	}
	else if(type=="SKOS-XL")
	{
		art_semanticturkey.openUrl("chrome://graph/content/showGraphSKOS-XL.html");	
	}
};



art_semanticturkey.associateEventsOnBrowserGraphicElements = function() {

	//Handler for the graph button
	document.getElementById("graphButton").addEventListener("command",art_semanticturkey.graphButtonManager,true);

	new art_semanticturkey.eventListener("projectOpened", art_semanticturkey.enableButton, null);
		
	new art_semanticturkey.eventListener("projectClosed", art_semanticturkey.enableButton, null);


	
};

art_semanticturkey.enableButton = function (event, eventObject) {

	//Event listener to enable/disable the graph button
	art_semanticturkey.Logger.debug("EVENT:"+event);
	art_semanticturkey.Logger.debug("eventObject.getProjectName():"+eventObject.getProjectName());
	art_semanticturkey.Logger.debug("eventObject.getType():"+eventObject.getType());
	type= eventObject.getType();
	if(event=="projectOpened")
		document.getElementById("graphButton").hidden = false;
	if(event=="projectClosed")
		document.getElementById("graphButton").hidden = true;

};

window.addEventListener("load",
		art_semanticturkey.associateEventsOnBrowserGraphicElements, true);

