/*
 * Developed by Alessio Leo.
 */

if ("undefined" == typeof (windowManager)) {
  var windowManager = {};
};

if (typeof art_semanticturkey == 'undefined')
  var art_semanticturkey = {};

if (typeof windowManager.eventManager == 'undefined') 
    windowManager.eventManager = {};

Components.utils.import("resource://graphservices/SERVICE_Graph.jsm", windowManager);
Components.utils.import("resource://graphmodules/graphRequests.jsm", windowManager);
Components.utils.import("resource://stservices/SERVICE_Cls.jsm", art_semanticturkey);
Components.utils.import("resource://stservices/SERVICE_Individual.jsm", art_semanticturkey);
Components.utils.import("resource://stservices/SERVICE_Synonyms.jsm", art_semanticturkey);
Components.utils.import("resource://stservices/SERVICE_OntoSearch.jsm",art_semanticturkey);
Components.utils.import("resource://stmodules/ResponseContentType.jsm", art_semanticturkey);
Components.utils.import("resource://stservices/SERVICE_SPARQL.jsm", art_semanticturkey);
Components.utils.import("resource://stservices/SERVICE_Metadata.jsm", art_semanticturkey);
Components.utils.import("resource://stmodules/Logger.jsm", art_semanticturkey);
Components.utils.import("resource://stmodules/StartST.jsm", art_semanticturkey);
Components.utils.import("resource://stmodules/ProjectST.jsm", art_semanticturkey);
Components.utils.import("resource://stservices/SERVICE_Annotation.jsm", art_semanticturkey);
Components.utils.import("resource://stservices/SERVICE_Projects.jsm", art_semanticturkey);
Components.utils.import("resource://stmodules/Preferences.jsm", art_semanticturkey);
Components.utils.import("resource://stmodules/PrefUtils.jsm", art_semanticturkey);
Components.utils.import("resource://stservices/SERVICE_Annotation.jsm", art_semanticturkey);
Components.utils.import("resource://stmodules/stEvtMgr.jsm", art_semanticturkey);
Components.utils.import("resource://stmodules/SemTurkeyHTTPLegacy.jsm");

var labelType, useGradients, nativeTextSupport, animate, fd, 
schemeId, nNodes, visitedNodes, maxNodes, freeze, project;

(function() {
  var ua = navigator.userAgent,
      iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
      typeOfCanvas = typeof HTMLCanvasElement,
      nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
      textSupport = nativeCanvasSupport 
        && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
  //I'm setting this based on the fact that ExCanvas provides text support for IE
  //and that as of today iPhone/iPad current text support is lame
  labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
  nativeTextSupport = labelType == 'Native';
  useGradients = nativeCanvasSupport;
  animate = !(iStuff || !nativeCanvasSupport);
})();

var Log = {
  elem: false,
  write: function(text){
    if (!this.elem) 
      this.elem = document.getElementById('log');
    this.elem.innerHTML = text;
    this.elem.style.left = (500 - this.elem.offsetWidth / 2) + 'px';
  }
};

windowManager.init = function(){
	//add the events
	windowManager.listenOnEvents();
  nNodes = new Array();
	freeze = 0;
  project = art_semanticturkey.CurrentProject.getProjectName();
	var json = windowManager.eventManager.graphFunction("start");
	initDirected(json);
};

windowManager.listenOnEvents = function() {
    new art_semanticturkey.eventListener("removedClass", art_semanticturkey.showWarning, null);
    new art_semanticturkey.eventListener("createdSubClass", art_semanticturkey.showWarning, null);
    new art_semanticturkey.eventListener("projectClosed", art_semanticturkey.showWarning, null);
    new art_semanticturkey.eventListener("projectOpened", art_semanticturkey.showWarning, null);
    new art_semanticturkey.eventListener("renamedClass", art_semanticturkey.showWarning, null);
};

art_semanticturkey.showWarning = function(event, eventObject) {
	if(event=="removedClass")
  {
    var node = fd.graph.getNode(eventObject.getClassRes().getURI());
    if(node)
    {
		  Log.write('warning, the class '+eventObject.getClassRes().getShow()+' has been removed');
      recursivelyRemove(node, node);
      fd.op.removeNode(node.id,
      {  
          type: 'fade:seq',  
          duration: 1000,  
          hideLabels: false,  
          transition: $jit.Trans.Quart.easeOut  
      });
    }
  }
	else if(event=="createdSubClass")
  {
		Log.write('warning, new Subclass created, graph might not be consinstent');
  }
  else if(event=="projectClosed")
  {
    Log.write('warning, working project has been closed, Graph operations forzen')
    freeze = 1;
  }
  else if(event=="projectOpened")
  {
    if(project==art_semanticturkey.CurrentProject.getProjectName())
    {
       location.reload();
    }
  }
  else if(event=="renamedClass")
  {
      var node = fd.graph.getNode(eventObject.getClassRes().getURI());
      if(node)
      {
         Log.write('warning, the class '+event.getOldClassName()+' has been renamed in '+event.getNewClassName);
      }
 
  }
};

countNodes=function()
{
  var i=0;
  var count =0;
  while(i<nNodes.length)
  {
    if(nNodes[i]!=null)
        count++;
    i++;
  }
  return count;
}

windowManager.eventManager.graphFunction = function(input) {
  response = windowManager.graphRequest(input, false, false);
  return response;
};

windowManager.eventManager.enable = function(event) {

  var graphButton = document.getElementById("graphButton");
  graphButton.hidden=false;
};


populateCheckboxList = function() {
  var toReturn = new Array();
  var objData = document.getElementById("objData");
  var dataType = document.getElementById("dataType");
  var connections = document.getElementById("connections");
  var prefLabels = document.getElementById("prefLabels");
  var altLabels = document.getElementById("altLabels");
  var hiddenLabels = document.getElementById("hiddenLabels");
  var labelsAsNodes = document.getElementById("labelsAsNodes");
  var labelRelations = document.getElementById("labelRelations");
  for (var i = 0; i<8; i++) {
    toReturn[i] = false; //set as false by default
  };

  if(objData.checked)
    toReturn[0] = true;
  if(dataType.checked)
    toReturn[1] = true;
  if(connections.checked)
    toReturn[2] = true;
  if(prefLabels.checked)
    toReturn[3] = true;
  if(altLabels.checked)
    toReturn[4] = true;
  if(hiddenLabels.checked)
    toReturn[5] = true;
  if(labelsAsNodes.checked)
    toReturn[6] = true;
  if(labelRelations.checked)
    toReturn[7] = true;
  return toReturn;

};
var lastNodeCheckedInfo;

/**
 * request to Server
 */

buildJson = function(input, more, isScheme, list, langTag)
{
   var to_return = 
   '{"input" : "'+input+
   '",\n"lang" : "SKOSXL'+
   '",\n"more" : "'+more+
   '",\n"isScheme" : "'+isScheme+
   '",\n"langTag" : "'+langTag+
   '",\n"objData" : "'+list[0].toString()+
   '",\n"dataType" : "'+list[1].toString()+
   '",\n"connections" : "'+list[2].toString()+
   '",\n"prefLabels" : "'+list[3].toString()+
   '",\n"altLabels" : "'+list[4].toString()+
   '",\n"hiddenLabels" : "'+list[5].toString()+
   '",\n"labelsAsNodes" : "'+list[6].toString()+ 
   '",\n"labelRelations" : "'+list[7].toString()+'"}';
   return to_return;
}

windowManager.graphRequest = function(input, more, isScheme) {
  if(freeze!=1)
  {
    var response;
    var list = new Array();
    list = populateCheckboxList();
    var langTag = document.getElementById('langTag').value;
    var jsonToSend = buildJson(input, more, isScheme, list, langTag);
    response = windowManager.Requests.Graph.graph(jsonToSend);
    var jsonString = response.getElementsByTagName("data")[0].getAttribute("result");
    var queryInfo = response.getElementsByTagName("data")[0].getAttribute("info");
    var nodeList = response.getElementsByTagName("data")[0].getAttribute("idList");
    if(input!="start")
      updateNodes(1, nodeList);
    lastNodeCheckedInfo = queryInfo;
    list = null;
    return jsonString;
  }
  else
  {
    window.alert("Graph operations frozen");
    return "[]";
  }
};

/*
*Stores the triples recieved for a clicked resource in the corresponding node
*/
setNodeInfos = function(node)
{
  
   var json = JSON.parse(lastNodeCheckedInfo);
   node.setData('info', json);
   
}

/*
* Returns true if the user wants to see the triples related to a given node
*/
showConnections = function()
{
    var connections = document.getElementById("connections");
    if(connections.checked)
      return true;
    else return false;

}

showLegend = function()
{
  window.open("legend.html","Legend","width=800, height=400");
};

removeFromArray = function(nodeToRemove)
{
   var i=0;
   while(i<nNodes.length)
   {
      if(nodeToRemove==nNodes[i])
        nNodes[i]=null;
      i++;
   }
}

addToArray = function(nodeToAdd)
{
   var i=0;
   while(i<nNodes.length)
   {
      if(nNodes[i]==null)
      {
        nNodes[i]=nodeToAdd;
        return;
      }
      i++;
   }
   nNodes[i]=nodeToAdd;
}

updateNodes = function(mode, json)
{

  var nodeArray;
  if(mode==0)//remove
  {
      nodeArray=json;
      for (var i = 0; i<nodeArray.length; i++) 
      {
          if(contains(nNodes, nodeArray[i]))
          {
              removeFromArray(nodeArray[i], nNodes);
          }
      };
  }
  if(mode==1)//add
  {
      nodeArray=JSON.parse(json);
      for(var i = 0; i<nodeArray.length; i++)
      {
          if(!contains(nNodes, nodeArray[i]))
          {
              addToArray(nodeArray[i], nNodes)
          }

      };
  }
}
recursivelyRemove = function(node, root)
{


  /************************************************************************
    NOTE:
    library functions like eachSubnode, eachSubgraph, etc., have a strange
    behaviour when doing some graph manipulation during their cycle, it's 
    impossible to remove edges or nodes during their execution, the removal
    has to be done outside the cycle.
  *************************************************************************/

  if(!node)
     return;
  else
  {
      var sonArray = new Array(maxNodes);
      var recArray = new Array(maxNodes);
      var edgeArray = new Array(maxNodes);
      var i=0;
      var j=0;
      var k=0;
      var topNode = root;
      node.eachSubnode(function(son) 
      {
        son.setData('removable', true, 'current');//marking each subnode as removable
        recArray[k]=son
        k++;
      }); 

      var l=0;
      while(l<k)
      {
        recursivelyRemove(recArray[l], topNode);
        l++;
      }
      recArray=null;


      node.eachSubnode(function(son)
      {
        var to_remove = true;
        son.eachAdjacency(function(adj)
        {
            /****************************************************************************
            I should remove a node only if it has not a parent that shouldn't be removed
            *****************************************************************************/
            if(adj.nodeTo.getData('removable')!=true && adj.nodeTo.id!=topNode.id)
            {
                  to_remove = false;
            }
        });

        if(node.id!=son.id && to_remove)
        {
          sonArray[i]=son.id;
          i++;
        }
        if(node.id!=son.id)
        {
          edgeArray[j]=son.id;
          j++;

        }
      });
      

      while(j>0)
      {
          fd.op.removeEdge([node.id, edgeArray[j-1]], {  
          type: 'replot',  
          duration: 0,  
          hideLabels: false,  
          transition: $jit.Trans.Quart.easeOut  
        }); 

        j--;
      }
      

      fd.op.removeNode(sonArray,
      {  
          type: 'replot',  
          duration: 0,  
          hideLabels: false,  
          transition: $jit.Trans.Quart.easeOut  
      });

      updateNodes(0, sonArray);

      sonArray=null;
      edgeArray=null;

  }

    
}

/*
* set all nodes removable flag to false.
*/
resetData = function(node)
{
    node.eachSubnode(function(son)
    {  
       son.setData('removable', false, 'current');
       resetData(son);
    });    
}

/*
* tells wheter or not a given String is already stored in an array
*/
contains= function(array, string) {
    for (var i = 0; i < array.length; i++) 
    {
        if (array[i] == string) 
        {
            return true;
        }
    }
    return false;
}


/*
* loads a new graph and defines the functions for its management
*/
initDirected = function(input)
{
   var json = JSON.parse(input);

   fd = new $jit.ForceDirected({

    //id of the visualization container
    injectInto: 'infovis',
    //Enable zooming and panning
    //by scrolling and DnD
    Navigation: {
      enable: true,
      type: 'Native',

      //Enable panning events only if we're dragging the empty
      //canvas (and not a node).
      panning: 'avoid nodes',
      zooming: 10 //zoom speed. higher is more sensible
    },
    // Change node and edge styles such as
    // color and width.
    // These properties are also set per node
    // with dollar prefixed data-properties in the
    // JSON structure.
    Node:  {
      overridable: true,
      type: 'square',
      dim: '11'
    },

    Edge: {
      overridable: true,
      color: '#23A4FF',
      lineWidth: 0.4,
      type: 'arrow'
    },
    
    //Add Tips
    
    Tips: {
      enable: true,
      onShow: function(tip, node) {
        //count connections
        var count = 0;
        node.eachAdjacency(function() { count++; });
        //display node info in tooltip
        tip.innerHTML = "<div class=\"tip-title\">" + node.name + "</div>"
          + "<div class=\"tip-text\"><b>connections:</b> " + count + "</div>";  
          if(showConnections())
          {       

            var html = "<h4>" + node.name + "</h4><b> connections:</b><ul><li>",
                list = [];

            if(node.getData('info')!=0)
            {
                var counter =0;
                while((node.getData('info'))[counter]!=undefined)
                {
                  var to_check = (node.getData('info'))[counter];
                  if(!contains(list, to_check))
                      list.push(to_check);
                  counter++;
                }
            }    
            node.eachAdjacency(function(adj)
            {
                var counter2=0;
                if((adj.nodeTo.getData('info'))!=0)
                {
                    while((adj.nodeTo.getData('info'))[counter2]!=undefined)
                    {
                       var to_check = (adj.nodeTo.getData('info'))[counter2].toString();
                       var splitted = to_check.split(" ");//i'll have the string in a 3 element array [sub, pred, obj]
                       if(((splitted[0]==node.name || (splitted[2]==node.name))))
                       {
                            if(!contains(list, to_check))
                               list.push(to_check);
                       }
                       counter2++;
                    }
   
                }
            });       

            $jit.id('inner-details').innerHTML = html+ list.join("</li><li>") + "</li></ul>";

          }
          else 
          {
             $jit.id('inner-details').innerHTML = "";
          }
      }
     
    },
    
    // Add node events
    Events: {
      enable: true,
      type: 'Native',
      //Change cursor style when hovering a node
      onMouseEnter: function() {
        fd.canvas.getElement().style.cursor = 'move';
      },
      onMouseLeave: function() {
        fd.canvas.getElement().style.cursor = '';
      },
      //Update node positions when dragged
      onDragMove: function(node, eventInfo, e) {
        var pos = eventInfo.getPos();
        node.pos.setc(pos.x, pos.y);
        fd.plot();
      },
      //Implement the same handler for touchscreens
      onTouchMove: function(node, eventInfo, e) {
        $jit.util.event.stop(e); //stop default touchmove event
        this.onDragMove(node, eventInfo, e);
      },
    
      //Add also a click handler to nodes
      onClick: function(node) {
        if(!node) return;

        else if(node.id.indexOf("More")>-1)
        {
            var parent = node.getData('parent');
            var nodeId = node.id;
            var isScheme;
            node.eachAdjacency(function(adj)
            {
              if(adj.nodeTo.getData('scheme')=='true')
              {
                isScheme=true;
              }

            });
            fd.op.removeEdge([nodeId, parent], {  
                  type: 'fade:seq',
                  duration: 1000,  
                  hideLabels: false,  
                  transition: $jit.Trans.Quart.easeOut  
              }); 
          
            fd.graph.removeAdjacence(nodeId, parent);


            fd.op.removeNode(nodeId, {  
                 type: 'fade:seq',  
                 duration: 1000,  
                 hideLabels: false,  
                 transition: $jit.Trans.Quart.easeOut  
            });
            var newJsonString = windowManager.graphRequest(parent, true, isScheme);
            var noResults=false;
            var newJson = JSON.parse(newJsonString);
            newJson.filter(function(item)
            {
                if(item.$noResults=='true')
                    noResults=true;
            });
            if(noResults)
                window.alert("no results");
            else
            {
              fd.op.sum(newJson, 
              {  
                  type: 'fade:seq',      
                  duration: 1000,  
                  hideLabels: false,  
                  transition: $jit.Trans.Quart.easeOut  
              });

              var nodeArray = new Array();
              visitedNodes=1;
              nodeArray=null;
              maxNodes= document.getElementById("maxNodes").value;
              $jit.id('openedNodes').innerHTML = "<h4> Opened Nodes: </h4>"+countNodes()+"/"+maxNodes;
              if(countNodes()>=maxNodes)
              {
                window.alert("warning, node limit reached");
              }

            }
        }
        else if(node.getData('scheme')=='true')
        {
          if(!node.selected)
          {
            node.selected=true;
            schemeId = node;
            var newJsonString = windowManager.graphRequest(node.id, false, true);
            var noResults=false;
            var newJson = JSON.parse(newJsonString);
            newJson.filter(function(item)
            {
                if(item.$noResults=='true')
                    noResults=true;
            });
            if(noResults)
                window.alert("no results");
            else
            {
               fd.op.morph(newJson, 
               {  
                    type: 'fade:seq',  
                    duration: 3000,  
                    hideLabels: false,  
                    transition: $jit.Trans.Quart.easeOut,
                    id: schemeId.id
                });

                visitedNodes=1;
                maxNodes= document.getElementById("maxNodes").value;
                $jit.id('openedNodes').innerHTML = "<h4> Opened Nodes: </h4>"+countNodes()+"/"+maxNodes;
                if(countNodes()>=maxNodes)
                {
                    window.alert("warning, node limit reached");
                }

            }        
          }
        
          setNodeInfos(node);
          if(showConnections())
          {
            // Build the right column relations list.
            // This is done by traversing the clicked node connections.

            var html = "<h4>" + node.name + "</h4><b> connections:</b><ul><li>",
                list = [];
            var count=0;
            node.eachAdjacency(function(adj){count++})
            if(node.getData('info')!=0)
            {
                var counter =0;
                while((node.getData('info'))[counter]!=undefined)
                {
                  var to_check = (node.getData('info'))[counter];
                  if(!contains(list, to_check))
                      list.push(to_check);
                  counter++;
                }
            }    
            node.eachAdjacency(function(adj)
            {
                var counter2=0;
                if((adj.nodeTo.getData('info'))!=0)
                {
                    while((adj.nodeTo.getData('info'))[counter2]!=undefined)
                    {
                       var to_check = (adj.nodeTo.getData('info'))[counter2].toString();
                       var splitted = to_check.split(" ");//i'll have the string in a 3 element array [sub, pred, obj]
                       if(((splitted[0]==node.name) || (splitted[2]==node.name)))
                       {
                            if(!contains(list, to_check))
                               list.push(to_check);
                       }
                       counter2++;
                    }
   
                }
            });       

            $jit.id('inner-details').innerHTML = html+ list.join("</li><li>") + "</li></ul>";

          }
          else 
          {
             $jit.id('inner-details').innerHTML = "";
          }

          

        }

        else
        {
            var newJsonString = windowManager.graphRequest(node.id, false, false);
            var noResults=false;
            var newJson = JSON.parse(newJsonString);
            newJson.filter(function(item)
            {
                if(item.$noResults=='true')
                    noResults=true;
            });
            if(noResults)
                window.alert("no results");
            else
            {
             fd.op.sum(newJson, 
             {  
                  type: 'fade:con',  
                  duration: 1000,  
                  hideLabels: false,  
                  transition: $jit.Trans.Quart.easeOut,
              });

              visitedNodes=1;
              maxNodes= document.getElementById("maxNodes").value;
              $jit.id('openedNodes').innerHTML = "<h4> Opened Nodes: </h4>"+countNodes()+"/"+maxNodes;
              if(countNodes()>=maxNodes)
              {
                  window.alert("warning, node limit reached");
              }

            }        
            setNodeInfos(node);
            if(showConnections())
            {
              // Build the right column relations list.
              // This is done by traversing the clicked node connections.

              var html = "<h4>" + node.name + "</h4><b> connections:</b><ul><li>",
                  list = [];
              var count=0;
              node.eachAdjacency(function(adj){count++})
              if(node.getData('info')!=0)
              {
                  var counter =0;
                  while((node.getData('info'))[counter]!=undefined)
                  {
                    var to_check = (node.getData('info'))[counter];
                    if(!contains(list, to_check))
                        list.push(to_check);
                    counter++;
                  }
              }    
              node.eachAdjacency(function(adj)
              {
                  var counter2=0;
                  if((adj.nodeTo.getData('info'))!=0)
                  {
                      while((adj.nodeTo.getData('info'))[counter2]!=undefined)
                      {
                         var to_check = (adj.nodeTo.getData('info'))[counter2].toString();
                         var splitted = to_check.split(" ");//i'll have the string in a 3 element array [sub, pred, obj]
                         if(((splitted[0]==node.name) || (splitted[2]==node.name)))
                         {
                              if(!contains(list, to_check))
                                 list.push(to_check);
                         }
                         counter2++;
                      }
     
                  }
              });       

              $jit.id('inner-details').innerHTML = html+ list.join("</li><li>") + "</li></ul>";

            }
            else 
            {
               $jit.id('inner-details').innerHTML = "";
            }

            

          }
   
        
      }

      
    },
    //Number of iterations for the FD algorithm
    iterations: 200,
    //Edge length
    levelDistance: 130,
    // Add text to the labels. This method is only triggered
    // on label creation and only for DOM labels (not native canvas ones).
    onCreateLabel: function(domElement, node){
      // Create a 'name' and 'close' buttons and add them
      // to the main node label
      if(node==schemeId)
      {
        setNodeInfos(node);
        visitedNodes=1;
        maxNodes= document.getElementById("maxNodes").value;
        $jit.id('openedNodes').innerHTML = "<h4> Opened Nodes: </h4>"+countNodes()+"/"+maxNodes;

      }
      var nameContainer = document.createElement('span'),
          closeButton = document.createElement('span'),
          style = nameContainer.style;
      nameContainer.className = 'name';
      var underscore = false
      if(node.name.length>10)
      {
          var popupInfo = node.name;
          nameContainer.innerHTML = '<u>'+node.name.substr(0,10)+'</u>';
          style.color = "#005B96";
          underscore=true;
          nameContainer.onclick = function()
          {
            window.alert(popupInfo);
          }

      }
      else
      {
        nameContainer.innerHTML = node.name;
      }
      if(node.id!='fake' && node.getData('scheme')!='true')
      {
       closeButton.className = 'close';
       closeButton.innerHTML = 'x';
      }
      domElement.appendChild(nameContainer);
      domElement.appendChild(closeButton);
      style.fontSize = "0.8em";
      if(!underscore)
        style.color = "#000";
      //Delete the nodes and its connections when
      //clicking the close button
      closeButton.onclick = function() 
      {
        node.setData('removable', true, 'current');
        recursivelyRemove(node, node);  
        visitedNodes=1;
        maxNodes= document.getElementById("maxNodes").value;
        $jit.id('openedNodes').innerHTML = "<h4> Opened Nodes: </h4>"+countNodes()+"/"+maxNodes;
      };
    },
    // Change node styles when DOM labels are placed
    // or moved.
    onPlaceLabel: function(domElement, node){
      var style = domElement.style;
      var left = parseInt(style.left);
      var top = parseInt(style.top);
      var w = domElement.offsetWidth;
      style.left = (left - w / 2) + 'px';
      style.top = (top + 10) + 'px';
      style.display = '';


    }

  });
  // load JSON data.
  fd.loadJSON(json);
  // compute positions incrementally and animate.
  fd.computeIncremental({
    iter: 40,
    property: 'end',
    onStep: function(perc){
      Log.write(perc + '% loaded...');
    },
    onComplete: function(){

      Log.write('done');
      fd.animate({
        modes: ['linear'],
        transition: $jit.Trans.Elastic.easeOut,
        duration: 2500
      });
    }
  });
  // end
}

window.addEventListener("load", windowManager.init, true);