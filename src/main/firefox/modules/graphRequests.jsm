Components.utils.import("resource://stmodules/SemTurkeyHTTPLegacy.jsm");

EXPORTED_SYMBOLS = [ "SemTurkeyHTTPLegacy", "Requests"];




/**
 * @class
 */
Requests = function(){};

/** VisualizationCODA Service
 * @class
 */
Requests.Graph = function(){};




// gestoreCODA service requests
Requests.Graph.serviceName = "graphService";
Requests.Graph.graphRequest = "graph";
