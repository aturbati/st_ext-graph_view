Components.utils.import("resource://graphmodules/graphRequests.jsm");
Components.utils.import("resource://stmodules/Logger.jsm");
Components.utils.import("resource://stmodules/Context.jsm");

EXPORTED_SYMBOLS = [ "SemTurkeyHTTPLegacy", "Requests" ];

var service = Requests.Graph;
var serviceName = service.serviceName;

/**
 * Graph method
 * 
 * @member Requests.Graph
 * @param json
 * @return
 */
function graph(json) {
	Logger.debug('[SERVICE_Graph.jsm] Graph.graph');
	var json = "json=" + json;  
	var contextAsArray = this.context.getContextValuesForHTTPGetAsArray();
	return SemTurkeyHTTPLegacy.GET(serviceName, service.graphRequest,json, contextAsArray);
}

//this return an implementation for GraphService with a specified context
service.prototype.getAPI = function(specifiedContext){
	var newObj = new service();
	newObj.context = specifiedContext;
	return newObj;
}
// Class SERVICE INITIALIZATION
service.prototype.graph = graph;
service.prototype.context = new Context();  // set the default context
service.constructor = service;
service.__proto__ = service.prototype;

